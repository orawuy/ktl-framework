package com.extosoft.keyword

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import groovy.time.TimeDuration
import groovy.time.TimeCategory
import groovy.transform.CompileStatic
import com.kms.katalon.core.util.KeywordUtil
import java.util.Date


import org.apache.poi.ss.usermodel.Color
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.xssf.usermodel.XSSFColor
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import org.apache.poi.xssf.usermodel.DefaultIndexedColorMap;
import org.apache.poi.hssf.util.HSSFColor
import java.text.SimpleDateFormat

public class UtilityKeyword {

	def ExcelKeyword excelKw = null
	def XSSFWorkbook xssfWb = null
	def Sheet sh = null
	def XSSFRow xssfRw = null
	def String screenShotTime = null

	@Keyword
	public String FW_GenerateDateTime(){
		KeywordUtil.logInfo('Call Keyword FW_GenerateDateTime')
		Date date = new Date()
		SimpleDateFormat sdf = new SimpleDateFormat ("yyyyMMdd_hhmmss")
		return sdf.format(date)
	}

	@Keyword
	public String FW_TestStartTime(){
		KeywordUtil.logInfo('Call Keyword FW_TestStartTime')
		Date date = new Date()
		GlobalVariable.testStartLongTime = System.currentTimeMillis()
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss")
		return sdf.format(date)
	}

	@Keyword
	public String FW_TestEndTime(){
		KeywordUtil.logInfo('Call Keyword FW_TestEndTime')
		Date date = new Date()
		GlobalVariable.testEndLongTime = System.currentTimeMillis()
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss")
		return sdf.format(date)
	}

	@Keyword
	public String FW_DateExecuted(){
		KeywordUtil.logInfo('Call Keyword FW_DateExecuted')
		Date date = new Date()
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy")
		return sdf.format(date)
	}
	@Keyword
	public String FW_GetCurrentDateVersion(){
		KeywordUtil.logInfo('Call Keyword FW_GetCurrentDateVersion')
		Date date = new Date()
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd")
		return sdf.format(date)
	}

	@Keyword
	public long FW_GetDurationTime(){
		KeywordUtil.logInfo('Call Keyword FW_GetDurationTime')
		KeywordUtil.logInfo("TestStartLongTime : " + GlobalVariable.testStartLongTime)
		KeywordUtil.logInfo("TestEndLongTime : " + GlobalVariable.testEndLongTime)
		long durationTime = GlobalVariable.testEndLongTime - GlobalVariable.testStartLongTime
		KeywordUtil.logInfo("DurationTime : " + durationTime)
		return durationTime
	}

	//	@Keyword
	//	public void FW_TestStartTransaction(String transactionName){
	//		KeywordUtil.logInfo('Call Keyword FW_TestStartTransaction')
	//		GlobalVariable.transactionName = GlobalVariable.currentTestCaseName + "_" + transactionName
	//		Date date = new Date()
	//		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss")
	//		excelKw = new ExcelKeyword()
	//		KeywordUtil.logInfo("********* FW_TestStartTransaction " + GlobalVariable.transactionName +" *********")
	//		GlobalVariable.testStartTime = sdf.format(date)
	//		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
	//		KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
	//		sh = excelKw.getExcelSheet(xssfWb, "LogTransactionTime")
	//		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowLogTransaction, 0, GlobalVariable.transactionName)
	//		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowLogTransaction, 1, GlobalVariable.testStartTime)
	//		KeywordUtil.logInfo("Set TransactionName : " + GlobalVariable.transactionName)
	//		KeywordUtil.logInfo("Set StartTime : " + GlobalVariable.testStartTime)
	//		excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
	//		KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
	//		KeywordUtil.logInfo("********************************************")
	//	}

	//	@Keyword
	//	public void FW_TestEndTransaction(){
	//		KeywordUtil.logInfo('Call Keyword FW_TestEndTransaction')
	//		Date date = new Date()
	//		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss")
	//		excelKw = new ExcelKeyword()
	//		KeywordUtil.logInfo("********* FW_TestEndTransaction " + GlobalVariable.transactionName +" *********")
	//		GlobalVariable.testEndTime = sdf.format(date)
	//		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
	//		KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
	//		sh = excelKw.getExcelSheet(xssfWb, "LogTransactionTime")
	//		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowLogTransaction, 2, GlobalVariable.testEndTime)
	//		KeywordUtil.logInfo("Set EndTime : " + GlobalVariable.testEndTime)
	//		excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
	//		KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
	//		KeywordUtil.logInfo("********************************************")
	//		GlobalVariable.currentRowLogTransaction = GlobalVariable.currentRowLogTransaction + 1
	//	}

	@Keyword
	public void FW_TestStartStep(String stepName){
		KeywordUtil.logInfo('Call Keyword FW_TestStartStep')
		GlobalVariable.stepName = GlobalVariable.runningStep + "# " + stepName
		GlobalVariable.captureName = GlobalVariable.runningStep + "# " + stepName
		Date date = new Date()
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		excelKw = new ExcelKeyword()
		KeywordUtil.logInfo("********* FW_TestStartStep " + GlobalVariable.stepName +" *********")
		GlobalVariable.testStartTime = sdf.format(date)
		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		sh = excelKw.getExcelSheet(xssfWb, "TestStep")
		//		GlobalVariable.currentTestCaseName=
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 0, GlobalVariable.currentTestCaseName.substring(0,5))
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 1, GlobalVariable.stepName)
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 4, GlobalVariable.testStartTime)
		KeywordUtil.logInfo("Set StepName : " + GlobalVariable.stepName)
		KeywordUtil.logInfo("Set StartTime : " + GlobalVariable.testStartTime)
		excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
		KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("********************************************")
	}

	@Keyword
	public void FW_TestStartStep(String stepName, String stepValue){
		KeywordUtil.logInfo('Call Keyword FW_TestStartStep')
		GlobalVariable.stepName = GlobalVariable.runningStep + "# " + stepName + " : " + stepValue
		GlobalVariable.captureName = GlobalVariable.runningStep + "# " + stepName
		Date date = new Date()
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		excelKw = new ExcelKeyword()
		KeywordUtil.logInfo("********* FW_TestStartStep " + GlobalVariable.stepName +" *********")
		GlobalVariable.testStartTime = sdf.format(date)
		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		sh = excelKw.getExcelSheet(xssfWb, "TestStep")
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 0, GlobalVariable.currentTestCaseName)
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 1, GlobalVariable.stepName)
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 4, GlobalVariable.testStartTime)
		KeywordUtil.logInfo("Set StepName : " + GlobalVariable.stepName)
		KeywordUtil.logInfo("Set StartTime : " + GlobalVariable.testStartTime)
		excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
		KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("********************************************")
	}

	@Keyword
	public void FW_TestEndStep(){
		KeywordUtil.logInfo('Call Keyword FW_TestEndStep')
		Date date = new Date()
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss")
		excelKw = new ExcelKeyword()
		KeywordUtil.logInfo("********* FW_TestEndStep " + GlobalVariable.stepName +" *********")
		GlobalVariable.testEndTime = sdf.format(date)
		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		sh = excelKw.getExcelSheet(xssfWb, "TestStep")
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 5, GlobalVariable.testEndTime)
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 6, GlobalVariable.MsgErr)
		KeywordUtil.logInfo("Set EndTime : " + GlobalVariable.testEndTime)
		KeywordUtil.logInfo("Set Msg Error : " + GlobalVariable.MsgErr)
		excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
		KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("********************************************")
		GlobalVariable.runningStep = GlobalVariable.runningStep + 1
		GlobalVariable.currentRowStep = GlobalVariable.currentRowStep + 1
		GlobalVariable.MsgErr = ""

	}



	@Keyword
	public void FW_CaptureScreenShot(){
		KeywordUtil.logInfo('Call Keyword FW_CaptureScreenShot')
		Date date = new Date()
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_hhmmssSSS")
		excelKw = new ExcelKeyword()
		KeywordUtil.logInfo("********* FW_CaptureScreenShot " + GlobalVariable.captureName +" *********")
		screenShotTime = sdf.format(date)
		GlobalVariable.outputScreenShotFileName = GlobalVariable.currentTestCaseName + "_" + GlobalVariable.captureName + "_" + screenShotTime + ".png"
		WebUI.takeScreenshot(GlobalVariable.outputReportFolderName + GlobalVariable.outputScreenShotFileName)
		xssfWb = excelKw.getWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("ReportResult Open : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		sh = excelKw.getExcelSheet(xssfWb, "TestStep")
		GlobalVariable.status = "PASSED"
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 2, GlobalVariable.status)
		excelKw.setValueToCellByIndex(sh, GlobalVariable.currentRowStep, 3, GlobalVariable.outputReportFolderName + GlobalVariable.outputScreenShotFileName)
		KeywordUtil.logInfo("Set ScreenShotName : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputScreenShotFileName)
		excelKw.saveWorkbook(GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName, xssfWb)
		KeywordUtil.logInfo("ReportResult Save : " + GlobalVariable.outputReportFolderName + GlobalVariable.outputReportFileName)
		KeywordUtil.logInfo("********************************************")
	}
}

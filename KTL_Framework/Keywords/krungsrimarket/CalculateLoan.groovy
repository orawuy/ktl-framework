package krungsrimarket

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.thoughtworks.selenium.webdriven.commands.Click

import internal.GlobalVariable

public class CalculateLoan {

	def WebKeyword web = new WebKeyword()
	def UtilityKeyword util = new UtilityKeyword()

	@Keyword
	def void LunchWebKrungsrimarket(){

		util.FW_TestStartStep("เปิดเวปกรุงศรี")
		web.FW_OpenBrowser("https://www.krungsri.com/bank/th/Other/Calculator/personal-revolving-calculator/loan-repayment-calculator.html")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(3, 1))
		//web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/Calculated from car price Tab'), "ตรวจสอบชื่อแถบ “คำนวณจากราคารถยนต์”","คำนวณจากราคารถยนต์")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectCarMakeId(){
		util.FW_TestStartStep("เลือกยี่ห้อ")
		web.FW_SelectOptionByValue(findTestObject('Krungsrimarket/Calculated from car price/select_CarMakeId'), "หน้าจอแสดงเลือกยี่ห้อ “BMW”", '5')
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), "หน้าจอแสดงเลือกยี่ห้อ “BMW”",findTestData('TC001').getValue(3, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void SelectLoanType(){
		util.FW_TestStartStep("เลือกประเภทสินเชื่อ")
		web.FW_SelectOptionByValue(findTestObject('Object Repository/Krungsri/Page_ l/select_'), "หน้าจอแสดงเลือกยี่ห้อ" , '4')
		// web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), "หน้าจอแสดงเลือกยี่ห้อ “BMW”",findTestData('TC00').getValue(3, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void InputCashCar(){
		util.FW_TestStartStep("กรอกราคารถ (ไม่รวม VAT)")
		web.FW_SetText(findTestObject("Object Repository/Krungsri/Page_ l/input__text_cash_car"), "หน้าจอแสดงราคารถ “5000000 บาท”", "5000000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsri/Page_ l/input__text_cash_car"), "หน้าจอแสดงราคารถ “5000000 บาท”", "5000000")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	
	@Keyword
	def void InputLoanYear(){
		util.FW_TestStartStep("อัตราดอกเบี้ยต่อปี")
		web.FW_SetText(findTestObject("Object Repository/Krungsri/Page_ l/input__text_loan_year"), "หน้าจอแสดงอัตราดอกเบี้ยต่อปี", "5")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsri/Page_ l/input__text_loan_year"), "หน้าจอแสดงอัตราดอกเบี้ยต่อปี", "5")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	
	

	@Keyword
	def void InputPercentVatCar(){
		util.FW_TestStartStep("กรอกเงินดาวน์")
		web.FW_SetText(findTestObject("Object Repository/Krungsri/Page_ l/input_  _text_percent_vat_car"), "หน้าจอแสดงเงินดาวน์ “500000 บาท”", "10")
		//web.clickElement("Object Repository/Krungsri/Page_ l/input_  _text_cash_vat_car")
		web.FW_Delay(10)
		//web.FW_Click(findTestObject("Object Repository/Krungsri/Page_ l/input_  _text_cash_vat_car", "คลิก"))
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsri/Page_ l/input_  _text_cash_vat_car"), "หน้าจอแสดงราคารถ “500000 บาท”", "500000")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectInstallmentPeriod(){
		util.FW_TestStartStep("เลือกระยะเวลาผ่อนชำระ")
		web.FW_SelectOptionByValue(findTestObject('Object Repository/Krungsri/Page_ l/select_122436486072'), "หน้าจอแสดงเลือกระยะเวลาผ่อนชำระ" , '72')
		web.FW_Delay(2)
		web.FW_CheckMessageBygetAttribute(findTestObject('Object Repository/Krungsri/Page_ l/select_122436486072'), "หน้าจอแสดงเลือกระยะเวลาผ่อนชำระ","72")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	
	@Keyword
	def void ClickCal(){
		util.FW_TestStartStep("กดปุ่มเริ่มคำนวณ")
		web.FW_Click(findTestObject('Object Repository/Krungsri/Page_ l/input__btn_cal'), "หน้าจอแสดงคำนวณคำนวณค่างวดต่อเดือน")
		//web.FW_Click(findTestObject('Object Repository/Krungsri/Page_ l/a__1'), "หน้าจอแสดคำนวณสินเชื่อรถยนต์")
		//web.FW_SelectOptionByValue(findTestObject('Object Repository/Krungsri/Page_ l/select_'), "หน้าจอแสดงประเภทสินเชื่อ “แคช ทู คาร์”", '4')
		web.FW_CheckMessageBygetAttribute(findTestObject('Object Repository/Krungsri/Page_ l/input__text_buy_loan'), "หน้าจอแสดงเลือกระยะเวลาผ่อนชำระ","4,500,000")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	
	@Keyword
	def void ChackBuyLoan(){
		util.FW_TestStartStep("ตรวจสอบยอดจัดเช่าซื้อ")
		//web.FW_Click(findTestObject('Object Repository/Krungsri/Page_ l/input__text_buy_loan'), "หน้าจอแสดงยอดจัดเช่าซื้อ")
		//web.FW_Click(findTestObject('Object Repository/Krungsri/Page_ l/a__1'), "หน้าจอแสดคำนวณสินเชื่อรถยนต์")
		//web.FW_SelectOptionByValue(findTestObject('Object Repository/Krungsri/Page_ l/select_'), "หน้าจอแสดงประเภทสินเชื่อ “แคช ทู คาร์”", '4')
		web.FW_CheckMessageBygetAttribute(findTestObject('Object Repository/Krungsri/Page_ l/input__text_buy_loan'), "หน้าจอแสดงยอดจัดเช่าซื้อ","4,500,000")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	
	@Keyword
	def void ChackLoanParMonth(){
		util.FW_TestStartStep("ตรวจสอบค่างวดต่อเดือน (รวม VAT)")
		//web.FW_Click(findTestObject('Object Repository/Krungsri/Page_ l/input__text_buy_loan'), "หน้าจอแสดงยอดจัดเช่าซื้อ")
		//web.FW_Click(findTestObject('Object Repository/Krungsri/Page_ l/a__1'), "หน้าจอแสดคำนวณสินเชื่อรถยนต์")
		//web.FW_SelectOptionByValue(findTestObject('Object Repository/Krungsri/Page_ l/select_'), "หน้าจอแสดงประเภทสินเชื่อ “แคช ทู คาร์”", '4')
		web.FW_CheckMessageBygetAttribute(findTestObject('Object Repository/Krungsri/Page_ l/input_ ( VAT)_text_loan_per_month'), "หน้าจอแสดงยอดจัดเช่าซื้อ","86,938")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	
	
	@Keyword
	def void SelectTab(){
		util.FW_TestStartStep("คำนวณสินเชื่อรถยนต์")
		web.FW_Click(findTestObject('Object Repository/Krungsri/Page_ l/a_'), "หน้าจอแสดคำนวณสินเชื่อรถยนต์")
		web.FW_Click(findTestObject('Object Repository/Krungsri/Page_ l/a__1'), "หน้าจอแสดคำนวณสินเชื่อรถยนต์")
		//web.FW_SelectOptionByValue(findTestObject('Object Repository/Krungsri/Page_ l/select_'), "หน้าจอแสดงประเภทสินเชื่อ “แคช ทู คาร์”", '4')
		//web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), "หน้าจอแสดงเลือกยี่ห้อ “BMW”",findTestData('TC001').getValue(3, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectCarTypeId(){

		util.FW_TestStartStep("เลือกประเภทรถ")
		web.FW_SelectOptionByValue(findTestObject('Krungsrimarket/Calculated from car price/select_CarTypeId'), "หน้าจอแสดงเลือกประเภทรถ “รถเก๋ง”", '1')
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarTypeIdValue'), "หน้าจอแสดงเลือกประเภทรถ “รถเก๋ง”",findTestData('TC001').getValue(2, 1))
		//		web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarTypeIdValue'), "หน้าจอแสดงเลือกประเภทรถ “รถเก๋ง”","500")

		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectCarModelId(){

		util.FW_TestStartStep("เลือกรุ่น")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/select_CarModelId'), "Krungsrimarket/Page_/select_CarModelId")
		web.FW_SelectOptionByValue(findTestObject('Krungsrimarket/Calculated from car price/select_CarModelId'), "หน้าจอแสดงเลือกรุ่น “M5”", '44')
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarModelIDValue'), "หน้าจอแสดงเลือกรุ่น “M5”",findTestData('TC001').getValue(4, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectYear(){

		util.FW_TestStartStep("เลือกปี")
		web.FW_SelectOptionByValue(findTestObject('Krungsrimarket/Calculated from car price/select_year'), "หน้าจอแสดงเลือกปี “2019”", '2019')
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/YearValue'), "หน้าจอแสดงเลือกปี “2019”",findTestData('TC001').getValue(5, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputCarPrince(){

		util.FW_TestStartStep("กรอกราคารถ(ไม่รวม VAT)")
		///**************
		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1CarPrice'), "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”", findTestData('TC001').getValue(6, 1))
		//web.FW_SetText(findTestObject('Krungsrimarket/Calculated from car price/select_year'), '5,000,000')
		//		web.FW_SetText(findTestObject('Pages/Checkout page/lnkCheckout'),'5,000,000')
		web.FW_Click(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1CarPrice'), "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”")
		//		String actualObject = WebUI.executeJavaScript('return $(\'input[id=txtTab1CarPrice]\')[0].value', ['null'])
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab1CarPrice]\')[0].value', "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”", "5,000,000")
		//		web.FW_CheckMessageByJS(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1CarPrice'), actualObject)
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputDownPaymentPercent(){

		util.FW_TestStartStep("กรอกเงินดาวน์")

		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1DownPaymentPercent'), "หน้าจอแสดงยอดเงินดาวน์ “500,000บาท” และ ยอดจัดเช่าซื้อ “4,500,000 บาท”", findTestData('TC001').getValue(7, 1))
		web.clickElement(findTestObject('Krungsrimarket/Calculated from car price/DownPaymentValue'))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab1DownPaymentPercent]\')[0].value', "หน้าจอแสดงยอดเงินดาวน์ “500,000บาท” และ ยอดจัดเช่าซื้อ “4,500,000 บาท”", findTestData('TC001').getValue(7, 1))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab1DownPaymentValue]\')[0].value', "หน้าจอแสดงยอดเงินดาวน์ “500,000บาท” และ ยอดจัดเช่าซื้อ “4,500,000 บาท”",findTestData('TC001').getValue(8, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void CheckTotalDawn(){

		util.FW_TestStartStep("ตรวจสอบเงินดาวน์")
		//WebUI.verifyElementVisible(findTestObject('Krungsrimarket/Calculated from car price/DownPaymentValue'))
		web.FW_VerifyElementVisible(findTestObject('Krungsrimarket/Calculated from car price/DownPaymentValue'))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab1DownPaymentValue]\')[0].value', "หน้าจอแสดงยอดเงินดาวน์ “500,000บาท”",findTestData('TC001').getValue(8, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()

	}

	@Keyword
	def void CheckHirePurchaseSales(){

		util.FW_TestStartStep("ตรวจสอบยอดจัดเช่าซื้อ")
		web.FW_VerifyElementVisible(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1Purchase'))

		web.FW_CheckMessageByJS('return $(\'input[id=txtTab1Purchase]\')[0].value', "หน้าจอแสดงยอดจัดเช่าซื้อ “4,500,000 บาท”",findTestData('TC001').getValue(9, 1))
		util.FW_CaptureScreenShot()

		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1CarPrice'), '10')
		util.FW_TestEndStep()

	}

	@Keyword
	def void ClickCalculatelLoan(){

		util.FW_TestStartStep("กดปุ่มคำนวณสินเชื่อ")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from car price/input__txtTab1CarPrice'), '10')
		web.FW_Click(findTestObject('Krungsrimarket/Calculated from car price/input__btnTab1CalculateLoan'), "หน้าจอแสงอัตราผ่อนชำระ(บาท/เดือน)(รวม vat)ที่72งวดเป็นเงิน “86,938 บาท”")
		web.FW_CheckMessage(findTestObject('Krungsrimarket/Calculated from car price/Installment rate per 72 M'), "หน้าจอแสงอัตราผ่อนชำระ(บาท/เดือน)(รวม vat)ที่72งวดเป็นเงิน “86,938 บาท”", findTestData('TC001').getValue(10, 1))
		//		findTestObject('Krungsrimarket/Calculated from car price/Installment rate per 72 M')
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void ClickCalculatedFromMonthlyTabMenu(){

		util.FW_TestStartStep("เลือกแถบคำนวณจากค่างวดต่อเดือน")

		web.FW_Click(findTestObject('Krungsrimarket/Calculated from monthly/Page_/Tab Calculated from monthly tab'), "หน้าจอแสดงข้อมูลคำนวณจากค่างวดต่อเดือน")
		web.FW_VerifyElementVisible(findTestObject('Object Repository/Krungsrimarket/Calculated from monthly/Page_/Monthly installment Check Page'))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void InputPeriodPayValue(){

		util.FW_TestStartStep("กรอกค่างวดต่อเดือน (รวม VAT)")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input_ ( VAT)_txtTab2PeriodPayValue'), '10000')
		//		web.FW_SetText(findTestObject('Object Repository/Pages X-Cart Demo/Page_X-Cart Demo store company  Catalog/input__login'), "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”", '10000')
		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input_ ( VAT)_txtTab2PeriodPayValue'), "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”", findTestData('TC002').getValue(2, 1))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab2PeriodPayValue]\')[0].value', "หน้าจอแสดงค่างวดต่อเดือน “10,000 บาท”", findTestData('TC002').getValue(2, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputDownPaymentValue(){

		util.FW_TestStartStep("กรอกเงินดาวน์")

		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input__txtTab2DownPaymentValue'), "หน้าจอแสดงเงินดาวน์ “120,000 บาท”", findTestData('TC002').getValue(3, 1))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab2DownPaymentValue]\')[0].value', "หน้าจอแสดงเงินดาวน์ “120,000 บาท”", findTestData('TC002').getValue(3, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void InputRateCutOFF(){
		util.FW_TestStartStep("กรอกดอกเบี้ย")
		web.FW_SetText(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input__txtTab2RateCutOff'), "หน้าจอแสดงดอกเบี้ย “5 %”", findTestData('TC002').getValue(4, 1))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab2RateCutOff]\')[0].value', "หน้าจอแสดงดอกเบี้ย “5 %”", findTestData('TC002').getValue(4, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void ClickButtonCalculateLoanMonthlyInstallments(){
		util.FW_TestStartStep("กดปุ่มคำนวณสินเชื่อ")
		web.FW_Click(findTestObject('Krungsrimarket/Calculated from monthly/Page_/input__btnTab2CalculateLoan'), "หน้าจอแสดงแสดงระยะเวลาการผ่อนชำระ/ราคารถสูงสุดที่แนะนำที่ 72 งวดเป็นเงิน “629,763บาท”")
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculated from monthly/Page_/Highest recommended car price per 72 Month'), "หน้าจอแสดงแสดงระยะเวลาการผ่อนชำระ/ราคารถสูงสุดที่แนะนำที่ 72 งวดเป็นเงิน “629,763บาท”", findTestData('TC002').getValue(5, 1))
		util.FW_CaptureScreenShot()
		//629,763
		util.FW_TestEndStep()
	}


	@Keyword
	def void ClickCalculateConditionalCreditsTAB(){
		util.FW_TestStartStep("เลือกแถบคำนวณสินเชื่อแบบระบุเงื่อนไข")
		web.FW_Click(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/Calculate conditional credits TAB'), "หน้าจอแสดงข้อมูลคำนวณสินเชื่อแบบระบุเงื่อนไข")
		web.FW_VerifyElementVisible(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/Calculate conditional credits TAB'))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}




	@Keyword
	def void InputCarPriceConditionalCredits(){
		util.FW_TestStartStep("กรอกราคารถ (ไม่รวม VAT)")

		web.FW_SetText(findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'), "หน้าจอแสดงราคารถ  2,500,000 บาท", findTestData('TC003').getValue(2, 1))
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'), "2,500,000")
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab3CarPrice]\')[0].value',  "หน้าจอแสดงราคารถ  2,500,000 บาท", findTestData('TC003').getValue(2, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
		//		CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'),
		//			'Input Car price', '500000')
	}

	@Keyword
	def void InputDownPaymentPercentConditionalCredits(){
		util.FW_TestStartStep("กรอกเงินดาวน์")

		web.FW_SetText(findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3DownPaymentPercent'), "หน้าจอแสดงยอดเงินดาวน์  500,000บาท และยอดจัดเช่าซื้อ  2,000,000 บาท",findTestData('TC003').getValue(3, 1))
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'), "2,500,000")
		web.FW_Click(findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3DownPaymentValue'), "หน้าจอแสดงยอดเงินดาวน์  500,000บาท และยอดจัดเช่าซื้อ  2,000,000 บาท")
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab3DownPaymentValue]\')[0].value', "หน้าจอแสดงยอดเงินดาวน์  500,000บาท และยอดจัดเช่าซื้อ  2,000,000 บาท", findTestData('TC003').getValue(5, 1))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab3Purchase]\')[0].value', "หน้าจอแสดงยอดเงินดาวน์  500,000บาท และยอดจัดเช่าซื้อ  2,000,000 บาท", findTestData('TC003').getValue(6, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
		//		CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'),
		//			'Input Car price', '500000')
	}


	@Keyword
	def void VerifyDownPaymentConditionalCredits(){
		util.FW_TestStartStep("ตรวจสอบยอดเงินดาวน์ ")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'), "2,500,000")

		web.FW_VerifyElementVisible(findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3DownPaymentValue'))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab3DownPaymentValue]\')[0].value', "หน้าจอแสดงยอดเงินดาวน์ “500,000บาท”", findTestData('TC003').getValue(5, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
		//		CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'),
		//			'Input Car price', '500000')
	}

	@Keyword
	def void VerifyPurchase(){
		util.FW_TestStartStep("ตรวจสอบยอดจัดเช่าซื้อ")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'), "2,500,000")

		web.FW_VerifyElementVisible(findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3Purchase'))
		web.FW_CheckMessageByJS('return $(\'input[id=txtTab3Purchase]\')[0].value', "หน้าจอแสดงยอดจัดเช่าซื้อ “2,000,000 บาท”", findTestData('TC003').getValue(6, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
		//		CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'),
		//			'Input Car price', '500000')
	}

	@Keyword
	def void InputRateCutOffConditionalCredits(){
		util.FW_TestStartStep("กรอกดอกเบี้ย")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'), "2,500,000")

		//		findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3RateCutOff')
		web.FW_SetText(findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3RateCutOff'), "หน้าจอแสดงดอกเบี้ย “5.00 %”", findTestData('TC003').getValue(7, 1))
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
		//		CustomKeywords.'com.extosoft.keyword.WebKeyword.FW_SetText'(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'),
		//			'Input Car price', '500000')
	}


	@Keyword
	def void ClickButtonCalculateLoanConditionalCredits(){
		util.FW_TestStartStep("กดปุ่มคำนวณสินเชื่อ")
		//		web.FW_SetText(findTestObject('Krungsrimarket/Calculate conditional credits/Page_/input__txtTab3CarPrice'), "2,500,000")
		web.FW_Click(findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/input__btnTab3CalculateLoan'), "หน้าจอแสงอัตราผ่อนชำระ(บาท/เดือน)(รวม vat)ที่72งวดเป็นเงิน 38,639 บาท ")
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/Page_/div_38639'), "หน้าจอแสงอัตราผ่อนชำระ(บาท/เดือน)(รวม vat)ที่72งวดเป็นเงิน 38,639 บาท ", findTestData('TC003').getValue(8, 1))
		//		38,639
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
		//		findTestObject('Object Repository/Krungsrimarket/Calculate conditional credits/Page_/Page_/div_38639')
	}

	@Keyword

	def void CloseCalculateLoanWebbrowser(){
		util.FW_TestStartStep("ปิดเวปกรุงศรี")

		web.FW_CloseBrowser("https://www.krungsri.com/bank/th/Other/Calculator/personal-revolving-calculator/loan-repayment-calculator.html")

		util.FW_TestEndStep()
	}
}

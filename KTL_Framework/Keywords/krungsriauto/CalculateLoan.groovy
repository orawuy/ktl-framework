package krungsriauto

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.extosoft.keyword.UtilityKeyword
import com.extosoft.keyword.WebKeyword
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable

public class CalculateLoan {


	def WebKeyword web = new WebKeyword()
	def UtilityKeyword util = new UtilityKeyword()

	@Keyword
	def void LunchWebKrungsriauto(){

		util.FW_TestStartStep("เปิดเวปกรุงศรีคำนวนสินเชื่อ")
		web.FW_OpenBrowser("https://www.krungsriauto.com/auto/home.html")
		//		web.FW_WaitForElementVisible(findTestObject('Krungsrimarket/Calculated from car price/h1'))
		//		web.FW_SetText22(findTestObject('Object Repository/Krungsrimarket/Calculated from car price/CarMakeIdValue'), findTestData('TC001').getValue(3, 1))

		web.FW_CheckMessage(findTestObject('Object Repository/Krungsriauto/CheckElementopenWeb'), "ตรวจสอบชื่อแถบ “สมัครสินเชื่อรถยนต์ ”","สมัครสินเชื่อรถยนต์")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void ClickPreliminaryCreditCalculation(){

		util.FW_TestStartStep("คลิกปุ่ม คำนวณสินเชื่อเบื้องต้น")

		web.FW_Click(findTestObject('Object Repository/Krungsriauto/Btn Preliminary credit calculation'), "คำนวณสินเชื่อ")
		web.FW_CheckMessage(findTestObject('Object Repository/Krungsriauto/Text Loan calculation'), "คำนวณสินเชื่อ", "คำนวณสินเชื่อ")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectLoanType(){
		util.FW_TestStartStep("เลือกประเภทสินเชื่อ")
		web.FW_SelectOptionByValue(findTestObject('Object Repository/Krungsri/Page_ l/select_'), "หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”", '4')
		//web.FW_CheckMessage(findTestObject('Object Repository/Krungsriauto/filter loan type'), "หน้าจอแสดงเลือกประเภทประเภทสินเชื่อ“กรุงศรี มอเตอร์ไซค์”", "กรุงศรี มอเตอร์ไซค์")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	
	




	@Keyword
	def void InputCarprice(){

		util.FW_TestStartStep("กรอกราคารถ (ไม่รวม VAT)")
		web.FW_SetText(findTestObject("Object Repository/Krungsriauto/Car price"), "หน้าจอแสดงราคารถ “60,000 บาท”", "60,000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Car price"), "หน้าจอแสดงราคารถ “60,000 บาท”", "60,000")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void InputDownPayment(){

		util.FW_TestStartStep("กรอกเงินดาวน์")
		web.FW_SetText(findTestObject("Object Repository/Krungsriauto/Input Down payment"), "หน้าจอแสดงยอดเงินดาวน์ “10,000 บาท”,หรือดาวน์  “16.67 %”", "10,000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input Down payment"), "หน้าจอแสดงยอดเงินดาวน์ “10,000 บาท”,หรือดาวน์  “16.67 %”", "10,000")
		web.FW_Click(findTestObject("Object Repository/Krungsriauto/Input Down percent"), "หน้าจอแสดงยอดเงินดาวน์ “10,000 บาท”,หรือดาวน์  “16.67 %”")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input Down percent"), "หน้าจอแสดงยอดเงินดาวน์ “10,000 บาท”,หรือดาวน์  “16.67 %”", "16.67")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void SelectInstallmentPeriod(){

		util.FW_TestStartStep("เลือก ระยะเวลาผ่อนชำระ ")
		web.FW_SelectOptionByValue(findTestObject("Object Repository/Krungsriauto/select_Installment period"), "หน้าจอแสดงระยะเวลาผ่อนชำระ “48”", "48")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/select_Installment period"), "หน้าจอแสดงระยะเวลาผ่อนชำระ “48”", "48")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void InterestRatePerMonth(){

		util.FW_TestStartStep("กรอกดอกเบี้ย")
		web.FW_SetText(findTestObject("Object Repository/Krungsriauto/Input_Interest rate per month"), "หน้าจอแสดงดอกเบี้ย “3.00 %”", "3")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input_Interest rate per month"), "หน้าจอแสดงดอกเบี้ย “3.00 %”", "3")
		web.FW_Click(findTestObject("Object Repository/Krungsriauto/Percent element"), "หน้าจอแสดงดอกเบี้ย “3.00 %”")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}

	@Keyword
	def void ClickCalculateloan(){

		util.FW_TestStartStep("กดปุ่มคำนวณสินเชื่อ")
		web.FW_Click(findTestObject("Object Repository/Krungsriauto/BtnCalculateloan"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input_BuyLoad"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”", "50,000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/loan_per_month"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”", "2,542")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}


	@Keyword
	def void VerifyInterestRatePerMonth(){

		util.FW_TestStartStep("ตรวจสอบยอดจัดเช่าซื้อ")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/Input_BuyLoad"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”", "50,000")
		web.FW_CheckMessageBygetAttribute(findTestObject("Object Repository/Krungsriauto/loan_per_month"), "หน้าจอแสงยอดจัดเช่าซื้อ “50,000 บาท”ค่างวดต่อเดือน (รวม VAT) “1,167”", "2,542")
		util.FW_CaptureScreenShot()
		util.FW_TestEndStep()
	}
	@Keyword

	def void CloseCalculateLoanWebbrowser(){
		util.FW_TestStartStep("ปิดเวปกรุงศรีคำนวณสินเชื่อ")

		web.FW_CloseBrowser("https://www.krungsriauto.com/auto/Calculator.html")

		util.FW_TestEndStep()
	}
	//	@Keyword
	//	def void ClickPreliminaryCreditCalculation(){
	//
	//		util.FW_TestStartStep("คลิกปุ่ม คำนวณสินเชื่อเบื้องต้น")
	//		web.FW_
	//
	//		util.FW_CaptureScreenShot()
	//		util.FW_TestEndStep()
	//	}



}

package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object reportFolderName
     
    /**
     * <p></p>
     */
    public static Object excelController
     
    /**
     * <p></p>
     */
    public static Object outputReportFileName
     
    /**
     * <p></p>
     */
    public static Object testfolder
     
    /**
     * <p></p>
     */
    public static Object waitPresentTimeout
     
    /**
     * <p></p>
     */
    public static Object testStartTime
     
    /**
     * <p></p>
     */
    public static Object testEndTime
     
    /**
     * <p></p>
     */
    public static Object dateExecuted
     
    /**
     * <p></p>
     */
    public static Object outputReportFolderName
     
    /**
     * <p></p>
     */
    public static Object currentTestCaseName
     
    /**
     * <p></p>
     */
    public static Object testCaseExecuteStatus
     
    /**
     * <p></p>
     */
    public static Object currentRowTestCase
     
    /**
     * <p></p>
     */
    public static Object testCaseName
     
    /**
     * <p></p>
     */
    public static Object tag
     
    /**
     * <p></p>
     */
    public static Object totalTestCasesPassed
     
    /**
     * <p></p>
     */
    public static Object totalTestCasesFailed
     
    /**
     * <p></p>
     */
    public static Object totalTestCasesExecuted
     
    /**
     * <p></p>
     */
    public static Object testCase
     
    /**
     * <p></p>
     */
    public static Object currentRowLogTransaction
     
    /**
     * <p></p>
     */
    public static Object testStartLogTransaction
     
    /**
     * <p></p>
     */
    public static Object currentRowStep
     
    /**
     * <p></p>
     */
    public static Object outputScreenShotFileName
     
    /**
     * <p></p>
     */
    public static Object stepName
     
    /**
     * <p></p>
     */
    public static Object transactionName
     
    /**
     * <p></p>
     */
    public static Object status
     
    /**
     * <p></p>
     */
    public static Object runningStep
     
    /**
     * <p></p>
     */
    public static Object stepValue
     
    /**
     * <p></p>
     */
    public static Object captureName
     
    /**
     * <p></p>
     */
    public static Object executeVersion
     
    /**
     * <p></p>
     */
    public static Object MsgErr
     
    /**
     * <p></p>
     */
    public static Object dateVersion
     
    /**
     * <p></p>
     */
    public static Object executeStatus
     
    /**
     * <p></p>
     */
    public static Object testStartLongTime
     
    /**
     * <p></p>
     */
    public static Object testEndLongTime
     
    /**
     * <p></p>
     */
    public static Object generateDateTime
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            reportFolderName = selectedVariables['reportFolderName']
            excelController = selectedVariables['excelController']
            outputReportFileName = selectedVariables['outputReportFileName']
            testfolder = selectedVariables['testfolder']
            waitPresentTimeout = selectedVariables['waitPresentTimeout']
            testStartTime = selectedVariables['testStartTime']
            testEndTime = selectedVariables['testEndTime']
            dateExecuted = selectedVariables['dateExecuted']
            outputReportFolderName = selectedVariables['outputReportFolderName']
            currentTestCaseName = selectedVariables['currentTestCaseName']
            testCaseExecuteStatus = selectedVariables['testCaseExecuteStatus']
            currentRowTestCase = selectedVariables['currentRowTestCase']
            testCaseName = selectedVariables['testCaseName']
            tag = selectedVariables['tag']
            totalTestCasesPassed = selectedVariables['totalTestCasesPassed']
            totalTestCasesFailed = selectedVariables['totalTestCasesFailed']
            totalTestCasesExecuted = selectedVariables['totalTestCasesExecuted']
            testCase = selectedVariables['testCase']
            currentRowLogTransaction = selectedVariables['currentRowLogTransaction']
            testStartLogTransaction = selectedVariables['testStartLogTransaction']
            currentRowStep = selectedVariables['currentRowStep']
            outputScreenShotFileName = selectedVariables['outputScreenShotFileName']
            stepName = selectedVariables['stepName']
            transactionName = selectedVariables['transactionName']
            status = selectedVariables['status']
            runningStep = selectedVariables['runningStep']
            stepValue = selectedVariables['stepValue']
            captureName = selectedVariables['captureName']
            executeVersion = selectedVariables['executeVersion']
            MsgErr = selectedVariables['MsgErr']
            dateVersion = selectedVariables['dateVersion']
            executeStatus = selectedVariables['executeStatus']
            testStartLongTime = selectedVariables['testStartLongTime']
            testEndLongTime = selectedVariables['testEndLongTime']
            generateDateTime = selectedVariables['generateDateTime']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
